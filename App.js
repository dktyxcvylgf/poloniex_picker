/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Suspense, lazy} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

const Tickers = lazy(() => import('./_app/Tickers'));

HomeScreen = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>О приложении</Text>
    </View>
  );
};

TickerScreen = () => {
  return (
    <SafeAreaView>
      <Suspense fallback={<ActivityIndicator />}>
        <Tickers />
      </Suspense>
    </SafeAreaView>
  );
};

const Tab = createBottomTabNavigator();

const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <NavigationContainer>
        <Tab.Navigator>
          <Tab.Screen name="Главная" component={HomeScreen} />
          <Tab.Screen name="Котировки" component={TickerScreen} />
        </Tab.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;
