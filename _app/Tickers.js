import React, {useEffect, useState} from 'react';
import {useIsFocused} from '@react-navigation/native';

import {View, Text, ActivityIndicator} from 'react-native';
import Axios from 'axios';
import {ScrollView} from 'react-native-gesture-handler';
import Item from './Item';
const sec = 5;

const Tickers = () => {
  const [resultsList, setResult] = useState();
  const [seconds, setSeconds] = useState(0);
  const focused = useIsFocused();

  getInfo = () => {
    let list = [];

    setError = (err) => {
      list.push(
        <View>
          <Text>ERROR!</Text>
          <Text>{err}</Text>
        </View>,
      );
    };

    updateList = (exchangeData) => {
      if (exchangeData.error) setError(exchangeData.error);
      else
        for (const key in exchangeData) {
          if (exchangeData.hasOwnProperty(key)) {
            const ticker = exchangeData[key];
            list.push(
              <Item
                key={key}
                data={{
                  key: key,
                  last: ticker.last,
                  highestBid: ticker.highestBid,
                  percentChange: ticker.percentChange,
                }}
              />,
            );
          }
        }
    };

    Axios.get('https://poloniex.com/public?command=returnTicker')
      .then((result) => {
        const exchangeData = result.data;
        updateList(exchangeData);
      })
      .then(() => {
        setResult(list);
      })
      .catch((err) => {
        console.log(err);
        setError(err);
        setResult(list);
      });
  };

  useEffect(() => {
    getInfo();
    const interval = setInterval(() => {
      focused && setSeconds((seconds) => seconds + sec);
    }, sec * 1000);
    return () => clearInterval(interval);
  }, [seconds, focused]);

  return <ScrollView>{resultsList ?? <ActivityIndicator />}</ScrollView>;
};

export default Tickers;
