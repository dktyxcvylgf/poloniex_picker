import React, {useRef, useEffect} from 'react';
import {Text, Animated} from 'react-native';
import _ from 'lodash';

const Item = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const data = props.data;
  const prevData = useRef();
  const anim = Animated.timing(fadeAnim, {
    toValue: 1,
    duration: 2000,
    useNativeDriver: false,
  });

  useEffect(() => {
    anim.start();
  }, [fadeAnim]);

  useEffect(() => {
    if (_.isEqual(data, prevData.current)) {
      // if (JSON.stringify(data) !== JSON.stringify(prevData.current)) {
      anim.reset();
      anim.start();
    }
    prevData.current = data;
  }, [data]);

  return (
    <Animated.View style={{opacity: fadeAnim}}>
      <Text style={{backgroundColor: 'gray'}}>{data.key}</Text>
      <Text>last: {data.last}</Text>
      <Text>highestBid: {data.highestBid}</Text>
      <Text>percentChange: {data.percentChange}</Text>
    </Animated.View>
  );
};

export default Item;
